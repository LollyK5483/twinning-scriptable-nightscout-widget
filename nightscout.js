//Primary's info: 
const nsUrl =`https://_____ .herokuapp.com`;  

// Diabestie's info:
const nsUrl2 =`https://______.herokuapp.com`; 

const glucoseDisplay = `mgdl`;
const dateFormat = `en-US`;

// Initialize Widget
let widget = await createWidget();
if (!config.runsInWidget) 
  {await widget.presentSmall();}

Script.setWidget(widget);
Script.complete();

// Build Widget
async function createWidget(items) 
  {const list = new ListWidget();

let header, glucose, iob, cob, updated, elapsed;

// telling the code to wait for instructions for the information it gets from nightscout
let nsDataV2 = await getNsDataV2(nsUrl);
let nsDataV2Diabestie = await getNsDataV2(nsUrl2);

// telling the code to wait for instructions on how to make the arrow
let directionString = await getDirectionString(nsDataV2.direction);

// title at top of widget
header = list.addText("Name".toUpperCase());
header.font = Font.mediumSystemFont(25);

//reading bg on each Nightscout
let bg = nsDataV2.bg; 
//primary ^
let bgDiabestie = nsDataV2Diabestie.bg;
//diabestie ^

let delta = nsDataV2.delta 

//fancy code for “let emoji be this thing with these conditions”
// Widget changes from dark red to bright red as you get out of range. Below 40 and above 250 are bright red. 

list.backgroundColor = new Color("hex code of choice");  //default here is white, black if your phone is on dark mode

if (bg <= 40)
{emoji = "🧃"; 
list.backgroundcolor = new Color("ff1f1f");}
else if (bg <= 45)
{emoji = "🧃";  
list.backgroundColor = new Color("f50000");}
else if (bg <= 50)
{emoji = "🧃";  
list.backgroundColor = new Color("d50000");}
else if (bg <= 55)
{emoji = "🧃";  
list.backgroundColor = new Color("b50000");}
else if (bg <= 60)
 {emoji = "🧃" ; 
list.backgroundColor = new Color("a50000");}
else if (bg <= 65)
 {emoji = "🧃";  
list.backgroundColor = new Color("850000");}
else if (bg <= 70)
{emoji = "🧃"; 
list.backgroundColor = new Color("550000");}
else if (bg == 90)
{emoji = "🎯"}
else if (bg == 100)
{emoji = "🦄";}
else if (bg <= 150) 
{emoji = "👍🏻";} 
else if (bg <= 180) 
{emoji = "🫣";} 
else if (bg <= 190)
{emoji = "🥴"; 
list.backgroundColor = new Color("550000");}
else if (bg <= 199)
{emoji = "🥴"; 
list.backgroundColor = new Color("850000");}
else if (bg == 200)
{emoji = "🦄🦄";
list.backgroundColor = new Color("a50000");}
else if (bg <= 210) 
 {emoji = "🥴";  
list.backgroundColor = new Color("a50000");}
else if (bg <= 220)
 {emoji = "🥴"; 
list.backgroundColor = new Color("b50000");}
else if (bg <=230)
{emoji = "🥴";  
list.backgroundColor = new Color("d50000");}
else if (bg <=240)
{emoji = "🥴";  
list.backgroundColor = new Color("f50000");}
else if (bg <=249)
{emoji = "🥴";  
list.backgroundColor = new Color("ff1f1f");}
else if (bg >= 250)
{emoji = "😵‍💫";
list.backgroundColor = new Color("ff1f1f");}
else if (bg == 300)
{emoji = "🦄🦄🦄";
list.backgroundColor = new Color("ff1f1f");}
else if (bg == 400)
{emoji = "🦄🦄🦄🦄";
list.backgroundColor = new Color("ff1f1f");}

// if they equal, this happens
//change emoji and/or color of widget with hex code of choice for when twinning happens. This color is teal. 
if (bg == bgDiabestie) 
{emoji += "👩🏼‍🤝‍👨🏻";
list.backgroundColor = new Color("0fb8b2");}

//basically, glucose plus arrow plus delta with emoji on next line
glucose = list.addText(bg + " " + directionString + "  " + delta);
glucose.font = Font.mediumSystemFont(20);
emojiLine = list.addText("" + emoji);
emojiLine.font = Font.mediumSystemFont(20);

// if looping, erase slashes so IOB and cob also appear on widget 
// if there’s not much room for emoji and IOB & cob then erase emoji info 
// iob = list.addText("" + nsDataV2.iob);
// iob.font = Font.mediumSystemFont(14);
// cob = list.addText("" + nsDataV2.cob);
// cob.font = Font.mediumSystemFont(14);

let updateTime = new Date(nsDataV2.mills).toLocaleTimeString(dateFormat, { hour: "numeric", minute: "numeric" })

updated = list.addText("" + updateTime)
updated.font = Font.mediumSystemFont(18);

list.refreshAfterDate = new Date(Date.now() + 10);
return list;}

//this reads the NS website for the primary
//if looping, erase slashes for IOB and con lines
async function getNsDataV2(baseUrl, token) {
    let url = baseUrl + "/api/v2/properties?&token=" + token;
    let data = await new Request(url).loadJSON();
    return {
           bg: data.bgnow.mean,
           direction: data.bgnow.sgvs[0].direction,
           delta: data.delta.display,
//         iob: data.iob.displayLine,
//         cob: data.cob.displayLine,
       mills: data.bgnow.mills};}

//arrow graphics and code
async function getDirectionString(direction) 

{let directionString
switch(direction) 
{case 'NONE':
directionString = '⇼';
break;
case 'DoubleUp':
directionString = '⇈';
break;
case 'SingleUp':
directionString = '↑';
break;
case 'FortyFiveUp':
directionString = '↗';
break;
case 'Flat':
directionString = '→';
break;
case 'FortyFiveDown':
directionString = '↘';
break;
case 'SingleDown':
directionString = '↓';
break;
case 'DoubleDown':
directionString = '⇊';
break;
case 'NOT COMPUTABLE':
directionString = '-';
break;
case 'RATE OUT OF RANGE':
directionString = '⇕';
break;
default:
directionString = '⇼';}
return directionString;}